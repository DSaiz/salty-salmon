﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ReactToGameover : MonoBehaviour {

    Rigidbody2D rb;
    SalmonMovement sm;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sm = GetComponent<SalmonMovement>();
    }

    void OnTriggerEnter2D(Collider2D waterfall)
    {
        if (waterfall.name == "Waterfall")
        {
            sm.EndGame();
        }else if(waterfall.name == "LaunchEnding"){
            Debug.Log("LaunchEnding");
            SceneManager.LoadScene("Ending");
        }
    }


}
