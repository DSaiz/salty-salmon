﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPointInWater : MonoBehaviour {


    public bool inWater = false;

    void OnTriggerEnter2D(Collider2D water)
    {
        if (water.CompareTag("Water"))
        {
            inWater = true;
        }
    }

    void OnTriggerExit2D(Collider2D water)
    {
        if (water.CompareTag("Water"))
        {
            inWater = false;
        }
    }
}
