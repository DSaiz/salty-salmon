﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SalmonMovement : MonoBehaviour
{

    [SerializeField]
    float shootForce = 3f;
    [SerializeField]
    float reloadTime = 1f;
    [SerializeField]
    int maxBullets = 20;
    [SerializeField]
    float _dragOnWater = 5f;
    [SerializeField]
    float _dragBubble = 2f;
    [SerializeField]
    float _gravityOffWater = 1.25f;
    [SerializeField]
    float _gravityBubble = 0.3f;
    [SerializeField]
    GameObject bulletPrefab;
    [SerializeField]
    Transform shootSpawn;
    [SerializeField]
    float maxSpeed = 50f;
    [SerializeField]
    GameObject ammo;
    [SerializeField]
    Sprite fondo, mask, fondoBW, maskBW;
    [SerializeField]
    GameObject salmonSpriteObject;
    [SerializeField]
    bool endGame = false;
    
    //[SerializeField]
    //GameObject bubbleObject;
    [SerializeField]
    GameObject splashPrefab;

    Rigidbody2D rb;

    bool intoTheWater = false;
    int currentBullets;
    float deltaReload = 0f;

    // Use this for initialization
    void Start()
    {
        endGame = false;
        rb = GetComponent<Rigidbody2D>();
        currentBullets = maxBullets;
        float percentage = ((float)currentBullets / (float)maxBullets);

        ammo.transform.FindChild("Bullets").GetComponent<Image>().fillAmount = 1;
    }


    Vector2 impulseVector;
    GameObject splashAuxObj;
    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale > 0 && !endGame)
        {

            if (Input.GetMouseButtonDown(0))
            {
                Vector2 mousePos = Input.mousePosition;
                mousePos = Camera.main.ScreenToWorldPoint(mousePos);

                impulseVector = (new Vector2(transform.position.x, transform.position.y) - mousePos).normalized * shootForce;


                if (!intoTheWater)
                {
                    if (currentBullets > 0)
                    {
                        ammo.transform.FindChild("Fondo").GetComponent<Image>().sprite = fondo;
                        ammo.transform.FindChild("Mask").GetComponent<Image>().sprite = mask;
                        UpdateBullets(-1);
                        rb.AddForce(impulseVector, ForceMode2D.Impulse);
                        Instantiate(bulletPrefab, shootSpawn.position, shootSpawn.parent.rotation);
                        //Destroy(bulletPrefab, 1);
                        GameMgr.Instance.audioMgr.PlayBullet();
                        splashAuxObj = Instantiate(splashPrefab, shootSpawn.position, Quaternion.Euler(0, 0, shootSpawn.eulerAngles.z - 90)) as GameObject;
                        splashAuxObj.GetComponent<Animator>().SetTrigger("Outside");
                        Destroy(splashAuxObj, 1f);
                    }
                    else
                    {
                        GameMgr.Instance.audioMgr.PlayAir();
                        ammo.transform.FindChild("Fondo").GetComponent<Image>().sprite = fondoBW;
                        ammo.transform.FindChild("Mask").GetComponent<Image>().sprite = maskBW;
                        //pequeño planeo al quedarse sin agua
                        impulseVector = (new Vector2(transform.position.x, transform.position.y) - mousePos).normalized * shootForce / 3;
                        //poner sprite de airecito         
                        splashAuxObj = Instantiate(splashPrefab, shootSpawn.position, Quaternion.Euler(0, 0, shootSpawn.eulerAngles.z - 90)) as GameObject;

                        splashAuxObj.GetComponent<Animator>().SetTrigger("Outside");

                        rb.AddForce(impulseVector, ForceMode2D.Impulse);

                    }
                }
                else
                {
                    rb.AddForce(impulseVector, ForceMode2D.Impulse);
                    if (shootSpawn.GetComponent<SpawnPointInWater>().inWater)
                    {
                        GameMgr.Instance.audioMgr.PlayWaterBullet();
                        Instantiate(bulletPrefab, shootSpawn.position, shootSpawn.parent.rotation).GetComponent<Bullet>().IntoWater();
                        splashAuxObj = Instantiate(splashPrefab, shootSpawn.position, Quaternion.Euler(0, 0, shootSpawn.eulerAngles.z - 90)) as GameObject;
                        splashAuxObj.GetComponent<Animator>().SetTrigger("Underwater");
                        Destroy(splashAuxObj, 1f);
                    }
                }

            }

            if (rb.velocity.magnitude > 0.01f)
            {
                salmonSpriteObject.transform.rotation = Quaternion.Euler(0, 0, Mathf.LerpAngle(salmonSpriteObject.transform.rotation.eulerAngles.z, Quaternion.FromToRotation(Vector3.right, rb.velocity).eulerAngles.z, Time.deltaTime));
            }
            else
            {
                salmonSpriteObject.transform.rotation = Quaternion.Lerp(salmonSpriteObject.transform.rotation, Quaternion.Euler(0, 0, 0), Time.deltaTime);
            }

            if (intoTheWater)
            {
                if (currentBullets < maxBullets)
                    ReloadTheBullets();
            }
        }

        //else
        //{
        //    if (Input.GetMouseButton(1))
        //    {
        //        ActiveBubble(true);
        //    }
        //    if (Input.GetMouseButtonUp(1))
        //    {
        //        ActiveBubble(false);
        //    }
        //}
    }

    //void ActiveBubble(bool act)
    //{
    //    if(bubbleObject != null)
    //    {
    //        bubbleObject.SetActive(act);
    //        rb.gravityScale = (act) ? _gravityBubble : _gravityOffWater;
    //        rb.drag = (act) ? _dragBubble : 0;
    //    }
    //}

    void ReloadTheBullets()
    {
        deltaReload += Time.deltaTime;
        if (deltaReload >= reloadTime)
        {
            deltaReload = 0;
            UpdateBullets(1);
        }
    }
    void UpdateBullets(int b)
    {
        currentBullets += b;
        Mathf.Clamp(currentBullets, 0, 5);

        float percentage = ((float)currentBullets / (float)maxBullets);

        if (currentBullets == 0)
        {
            ammo.transform.FindChild("Fondo").GetComponent<Image>().sprite = fondoBW;
            ammo.transform.FindChild("Mask").GetComponent<Image>().sprite = maskBW;

        }
        else
        {
            ammo.transform.FindChild("Fondo").GetComponent<Image>().sprite = fondo;
            ammo.transform.FindChild("Mask").GetComponent<Image>().sprite = mask;
            if (currentBullets <= 7)
            {
                ammo.transform.FindChild("Bullets").GetComponent<Image>().color = new Color(255f, 0f, 0f, 1f);
            }
            else if (currentBullets > 7)
            {
                ammo.transform.FindChild("Bullets").GetComponent<Image>().color = new Color(255f, 255f, 255f, 1f);
            }

        }
        ammo.transform.FindChild("Bullets").GetComponent<Image>().fillAmount = percentage;

    }
    public void EnterTheWater()
    {
        if (!intoTheWater)
        {
            //Debug.Log(rb.velocity.y);
            if (Mathf.Abs(rb.velocity.y) >= 6)
            {
                splashAuxObj = Instantiate(splashPrefab, new Vector3(transform.position.x, transform.position.y - 1.7f, 0), Quaternion.identity) as GameObject;
                splashAuxObj.GetComponent<Animator>().SetTrigger("Splash");
                GameMgr.Instance.audioMgr.PlaySplash();
                Destroy(splashAuxObj, 1f);
            }
            intoTheWater = true;
            rb.gravityScale = 0f;
            rb.drag = _dragOnWater;
        }
    }

    public void ExitTheWater()
    {
        intoTheWater = false;
        rb.gravityScale = _gravityOffWater;
        rb.drag = 0f;
    }

    public void EndGame()
    {
        if (!endGame)
        {
            Vector2 impulsoFinal = Vector2.one * 15;
            rb.AddForce(impulsoFinal, ForceMode2D.Impulse);
        }
        endGame = true;
    }
}
