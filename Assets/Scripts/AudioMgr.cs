﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioMgr : MonoBehaviour
{

    public AudioSource intro, loop, menu;
    AudioSource sfx;

    [SerializeField]
    AudioClip tin;

    [SerializeField]
    AudioClip collapse;

    [SerializeField]
    AudioClip splash;

    [SerializeField]
    AudioClip[] bear;

    [SerializeField]
    AudioClip swan;

    [SerializeField]
    AudioClip[] bullet;

    [SerializeField]
    AudioClip[] waterBullet;

    [SerializeField]
    AudioClip[] air;

    [SerializeField]
    AudioClip miniSplash;

    [SerializeField]
    AudioClip[] deaths;

    GameObject whale;

    private void Awake()
    {
        sfx = GetComponent<AudioSource>();
    }

    void PlayClip(AudioClip clip, float volume = 1)
    {
        sfx.pitch = Random.Range(0.8f, 1.2f);
        sfx.PlayOneShot(clip, volume);
    }

    public void PlayTin()
    {
        PlayClip(tin);
    }

    public void PlayCollapse()
    {
        PlayClip(collapse, 0.5f);
    }

    public void PlaySplash()
    {
        PlayClip(splash);
    }

    public void PlaySwan()
    {
        PlayClip(swan);
    }

    public void PlayBear()
    {
        PlayClip(bear[Random.Range(0, bear.Length - 1)]);
    }

    public void PlayBullet()
    {
        PlayClip(bullet[Random.Range(0, bullet.Length - 1)]);
    }

    public void PlayWaterBullet()
    {
        PlayClip(waterBullet[Random.Range(0, bullet.Length - 1)]);
    }

    public void PlayAir()
    {
        PlayClip(air[Random.Range(0, bullet.Length - 1)]);
    }

    public void PlayMiniSplash()
    {
        PlayClip(miniSplash);
    }

    public void CheckTrack()
    {
        SceneTrack sTrack = GameObject.Find("SceneTrack").GetComponent<SceneTrack>();
        if (sTrack != null) Invoke(sTrack.m_CallToAudioMgr, 0);
    }

    public void ToMenu()
    {
        Debug.Log("ToMenu");
        StopCoroutine("Intro_Loop");
        intro.Stop();
        loop.loop = false;
        loop.Stop();
        menu.Play();
    }

    public void ToLevel()
    {
        Debug.Log("ToLevel");
        menu.Stop();
        StartCoroutine("Intro_Loop");
    }

    IEnumerator Intro_Loop()
    {
        intro.Play();
        yield return new WaitUntil(() => !intro.isPlaying);
        loop.loop = true;
        loop.Play();
    }

    public void DeathByAnemona()
    {
        PlayClip(deaths[0]);
    }

    public void DeathByWhale()
    {
        PlayClip(deaths[1]);
    }

    public void DeathBySwan()
    {
        PlayClip(deaths[2]);
    }

    public void DeathByJellyfish()
    {
        PlayClip(deaths[3]);
    }

    public void DeathByBear()
    {
        PlayClip(deaths[4]);
    }

    public void DeathByNet()
    {
        PlayClip(deaths[5]);
    }

}
