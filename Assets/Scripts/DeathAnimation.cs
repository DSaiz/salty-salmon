﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathAnimation : MonoBehaviour {

    [SerializeField]
    protected AnimationCurve[] animationCurves;
    [SerializeField]
    protected float animationTime = 1f;
    protected float timer;
    

    public virtual void StartAnimation()
    {
        StartCoroutine("DoAnimation");
        timer = 0;
    }

    protected virtual IEnumerator DoAnimation()
    {
        return null;
    }

}
