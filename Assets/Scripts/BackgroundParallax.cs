﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundParallax : MonoBehaviour {

    public Transform [] backgrounds;
    private float[] parallaxScales; //Proporción de los movimientos de la cámara
    public float smoothing=1; //Velocidad de parallax (>0)

    private Transform cam; //Main camera transform
    private Vector3 previousCamPos; //la posición de la cámara en el frame anterior


    void Awake()
    {
        //definir la referencia de la cámera
        cam = Camera.main.transform;
    }


	// Use this for initialization
	void Start () {
        
        previousCamPos = cam.position;
        parallaxScales = new float[backgrounds.Length];
        //asignamos los parallaxScales
        for(int i = 0; i < backgrounds.Length; i++)
        {
            parallaxScales[i] = backgrounds[i].position.z * -1;
        }
	}
	
	// Update is called once per frame
	void Update () {

	    for (int i = 0; i < backgrounds.Length; i++)
        {
            //el parallax es el opuesto del movimiento de la cámara
            
            float parallax=(previousCamPos.x-cam.position.x)*parallaxScales[i];

            //Definir el target al que seguir
            float backgroundTargetPosX = backgrounds[i].position.x + parallax;

            //Creamos una targetPosition (la posición actual del background)
            Vector3 backgroundTargetPos = new Vector3(backgroundTargetPosX, backgrounds[i].position.y, backgrounds[i].position.z);

            //Suavizado
            backgrounds[i].position = Vector3.Lerp(backgrounds[i].position, backgroundTargetPos, smoothing*Time.deltaTime);

        }

        //Dejamos la anterior posición a la posición final
        previousCamPos = cam.position;
	}
}
