﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour {

    public ChangeScenesUI ChangeScenesUI;
    public GameObject Salty;
    public GameObject Sweety;

    public GameObject main;
    public GameObject credits;

    public GameObject bulletPlay;
    public GameObject bulletEnding;
    public GameObject bulletCredits;
    public GameObject bulletQuit;

    private Vector3 positionInicial;
    private Coroutine coroutine;

    private float waitTime = 0.6f;

    private void Awake()
    {
        positionInicial = Salty.transform.position;
    }

    public void ClickPlay()
    {
        Salty.GetComponent<Animator>().Play("Salty_Main_Menu", -1, 0f);
        PlayerPrefs.SetInt("FirstPlay", 1);
        bulletPlay.SetActive(true);
        Sweety.SetActive(true);
        coroutine = StartCoroutine(WaitPlay(waitTime));
    }

    public void ClickEnding()
    {
        Salty.GetComponent<Animator>().Play("Salty_Main_Menu", -1, 0f);
        bulletEnding.SetActive(true);
        coroutine = StartCoroutine(WaitEnding(waitTime));
    }

    public void ClickCredits()
    {
        Salty.GetComponent<Animator>().Play("Salty_Main_Menu", -1, 0f);
        bulletCredits.SetActive(true);
        coroutine = StartCoroutine(WaitCredits(waitTime));
    }

    public void ClickQuit()
    {
        Salty.GetComponent<Animator>().Play("Salty_Main_Menu", -1, 0f);
        bulletQuit.SetActive(true);
        ChangeScenesUI.OnQuitScene(true);
    }

    public void ClickBackCredits()
    {
        Salty.GetComponent<Animator>().Play("Salty_Back", -1, 0f); 
        main.SetActive(true);
        credits.SetActive(false);
    }

    IEnumerator WaitPlay(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        ChangeScenesUI.OnChangeScene("MainScene");
        ChangeScenesUI.DestroyGameMgr();
    }

    IEnumerator WaitEnding(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        ChangeScenesUI.OnChangeScene("Ending");
    }

    IEnumerator WaitCredits(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        bulletCredits.SetActive(false);
        yield return new WaitForSeconds(waitTime);
        credits.SetActive(true);
        main.SetActive(false);
    }

    public void SetPositionSalty()
    {
        Salty.transform.position = positionInicial;
    }
}
