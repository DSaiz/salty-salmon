﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class WhaleMovement : MonoBehaviour
{
    [SerializeField]
    float _timeToBeginWhale = 2f;
    [SerializeField]
    float Velocidad;
    [SerializeField]
    Vector3 Objetivo = new Vector3();
    [SerializeField]
    AnimationCurve scaleCurve;

    float timer;

    Rigidbody2D rb;

    public void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Invoke("MoveWhale", _timeToBeginWhale);
    }

    private void Update()
    {
        timer += Time.deltaTime;
        transform.localScale = new Vector3(transform.localScale.x, scaleCurve.Evaluate(timer), transform.localScale.z);
    }

    public void MoveWhale()
    {
        #region Movimiento hacia el objetivo
        Vector3 dir = new Vector3();
        dir = Objetivo - transform.position;

        dir.Normalize();

        rb.velocity = dir * Velocidad;
        #endregion
    }
}
