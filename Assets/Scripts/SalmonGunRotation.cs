﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SalmonGunRotation : MonoBehaviour {
    
    void LateUpdate()
    {
        RotateGus();
    }


    public void RotateGus()
    {
        Vector2 mousePos = new Vector2();
        //Debug.Log(mousePos);
        if (SceneManager.GetActiveScene().name != "MainMenu")
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
        else
        {
            mousePos = Input.mousePosition;
        }
        print(mousePos - (Vector2)transform.position);
        Quaternion rotation = Quaternion.FromToRotation(Vector2.right, mousePos - (Vector2)transform.position);
        transform.rotation = rotation;
    }
}
