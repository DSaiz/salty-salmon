﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    [SerializeField]
    SpriteRenderer spr;
    [SerializeField]
    Sprite[] sprites;
    [SerializeField]
    float _dragOnWater = 5f;
    [SerializeField]
    float speed = 10f;
    [SerializeField]
    Color transpColor;

    Rigidbody2D rb;

    bool intoTheWater = false;


    float deltaAnimation = 0f;
    float deltaDisable = 0f;
    bool animateBullet = false;

    public float secondsToDestroy=1f;
	
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.right * speed;
        Destroy(gameObject, secondsToDestroy);

    }

    // Update is called once per frame
    void Update ()
    {
        AnimateBullet();

    }


    void AnimateBullet()
    {
        deltaAnimation += Time.deltaTime;
        deltaDisable += Time.deltaTime;
        if (deltaAnimation >= 0.05f)
        {
            if (!intoTheWater)
            {
                spr.sprite = (animateBullet) ? sprites[0] : sprites[1];
                deltaAnimation = 0f;
                animateBullet = !animateBullet;
            }
            else
            {
                spr.sprite = (animateBullet) ? sprites[2] : sprites[3];
                spr.color = Color.Lerp(Color.white, transpColor, deltaDisable / 1f);
                deltaAnimation = 0f;
            }
        }
    }

    public void IntoWater()
    {
        intoTheWater = true;
        GameMgr.Instance.audioMgr.PlayMiniSplash();
        rb.drag = _dragOnWater;
        rb.gravityScale = 0f;
        deltaDisable = 0f;
        AnimateBullet();
        Destroy(gameObject, 1f);
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.CompareTag("Water"))
        {
            IntoWater();
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if(coll.CompareTag("Water"))
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        Destroy(gameObject);
    }


}
