﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rockCollission : MonoBehaviour
{

    public float speed = 1.0f; //how fast it shakes
    public float amount = 1.0f; //how much it shakes
    float originalPosition;
    bool derrumbe = false;
    bool killed = false;
    void Start()
    {
        originalPosition = this.transform.position.x;
    }
    void Update()
    {

        if (derrumbe)
        {
            this.transform.position = new Vector3(this.transform.position.x + Mathf.Sin(Time.time * speed) * amount, this.transform.position.y - 0.05f);
            this.transform.GetComponent<PolygonCollider2D>().offset = new Vector2(this.GetComponent<PolygonCollider2D>().offset.x, this.GetComponent<PolygonCollider2D>().offset.y - 0.05f);
            //this.transform.parent.GetComponent<Collider2D>().offset.y=  this.transform.position.y - 0.2f ;

            if (this.transform.position.y < -30)
            {
                Destroy(this.transform.parent.gameObject);
            }

        }

    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log(collision.gameObject.name);
        if (collision.name == "Teeth1")
        {
            GameMgr.Instance.audioMgr.PlayCollapse();
            derrumbe = true;
            if (!killed)
            {
                killed = true;
                if (this.transform.parent.FindChild("Dancingbear 1") != null)
                {

                    DiesToBullet db = this.transform.parent.FindChild("Dancingbear 1").GetComponent<DiesToBullet>();
                 
                    db.killObject();
                    //Destroy(db);
                }
            }
        }
    }
}
