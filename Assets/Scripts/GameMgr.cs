﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameMgr : MonoBehaviour
{
    #region Gestion del singleton

    private static GameMgr _instance;

    public static GameMgr Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.Log("GameMgr no inicializado.");
                return null;
            }
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance != null)
        {
            Debug.Log("Hay dos componentes en la e..");
            Destroy(this.gameObject);
            return;
        }
        _instance = this;
    }

    void OnDestroy()
    {
        if (_instance == this)
        {
            _instance = null;
        }
    }

    #endregion

    #region Variables públicas
    public SceneMgr sceneMgr;
    public AudioMgr audioMgr;

    #endregion

    [SerializeField]
    GameObject GameOverUI;

    [SerializeField]
    GameObject PauseButton;

    [SerializeField]
    GameObject AmmoBar;

    [SerializeField]
    GameObject pauseCanvas;

    [SerializeField]
    Texture2D mouseTexture;

    List<Button> buttons;

    bool paused = false;

    public void GameOver()
    {
        Time.timeScale = 0f;
        GameOverUI.SetActive(true);
        PauseButton.SetActive(false);
        AmmoBar.SetActive(false);
        buttons = new List<Button>(GameOverUI.GetComponentsInChildren<Button>());
        buttons.ForEach(c => c.interactable = false);
        StartCoroutine("ActivateButtons");
    }

    public void Retry()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        PlayerPrefs.SetInt("FirstPlay", 0);
        GameOverUI.SetActive(false);
        PauseButton.SetActive(true);
        AmmoBar.SetActive(true);

    }

    public void DestroyGameMgr()
    {
        Destroy(gameObject);
    }

    IEnumerator ActivateButtons()
    {
        yield return new WaitForSecondsRealtime(0.7f);
        buttons.ForEach(c => c.interactable = true);
        buttons.Clear();
    }

    public void Pause()
    {
        if (!paused)
        {
            Debug.Log("ña");
            paused = true;
            Time.timeScale = 0f;
            PauseButton.SetActive(false);
            AmmoBar.SetActive(true);
            pauseCanvas.SetActive(true);

        }
        else
        {
            Debug.Log("ño");
            paused = false;
            Time.timeScale = 1f;
            PauseButton.SetActive(true);
            AmmoBar.SetActive(true);
            pauseCanvas.SetActive(false);

        }

    }

    void Start()
    {
        Cursor.SetCursor(mouseTexture, new Vector2(mouseTexture.width / 2, mouseTexture.height / 2), CursorMode.ForceSoftware);
    }

}
