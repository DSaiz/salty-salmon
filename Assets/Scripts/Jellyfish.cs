﻿using UnityEngine;

public class Jellyfish : MonoBehaviour
{


    [SerializeField]
    float minY;
    [SerializeField]
    float maxY;

    [SerializeField]
    float patrolTime;

    AnimationCurve patrol;
    float timer;

    private void Start()
    {
        patrol = new AnimationCurve();
        patrol.AddKey(0,minY);
        patrol.AddKey(patrolTime, maxY);
        patrol.preWrapMode = WrapMode.PingPong;
        patrol.postWrapMode = WrapMode.PingPong;
    }

    private void Update()
    {
        timer += Time.deltaTime;
        Vector3 v = transform.position;
        v.y = patrol.Evaluate(timer);
        transform.position = v;
    }


}
