﻿using UnityEngine;
using System.Collections;

public class CameraMgr : MonoBehaviour
{

    public GameObject player;       //Public variable to store a reference to the player game object
    public Camera cam;

    private float offset;         //Private variable to store the offset distance between the player and camera

    // Use this for initialization
    void Start()
    {
        //Calculate and store the offset value by getting the distance between the player's position and camera's position.
        offset = cam.transform.position.x - player.transform.position.x;
    }

    // LateUpdate is called after Update each frame
    void LateUpdate()
    {
        // Set the position of the camera's transform to be the same as the player's, but offset by the calculated offset distance.
        float x = player.transform.position.x + offset;
        Vector3 vec = new Vector3(player.transform.position.x, cam.transform.position.y, -10f);
        cam.transform.position = vec;
    }
}