﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IniChapter : MonoBehaviour {
    
    [SerializeField]
    float timeToStart = 6.5f;

    Animator anim;
    float deltaTimeScale = 0;
	// Use this for initialization
	void Start ()
    {
        anim = GetComponent<Animator>();
        if(PlayerPrefs.GetInt("FirstPlay") == 1)
        {
            Time.timeScale = 0;
            anim.SetTrigger("Start");
        }
        else
        {
            anim.SetTrigger("None");
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        deltaTimeScale += Time.unscaledDeltaTime;
        if(deltaTimeScale >= timeToStart)
        {
            Time.timeScale = 1f;
            this.enabled = false;
        }	
	}
}
