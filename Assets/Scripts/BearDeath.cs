﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BearDeath : DeathAnimation {

    List<SpriteRenderer> sp;
    float baseY;
    List<int> baseOrder;

    public string OnDeath;

    public override void StartAnimation()
    {
        sp = new List<SpriteRenderer>(GetComponentsInChildren<SpriteRenderer>());
        baseY = transform.position.y;
        baseOrder = new List<int>();
        foreach(SpriteRenderer s in sp)
        {
            baseOrder.Add(s.sortingOrder);
        }
        base.StartAnimation();
    }

    protected override IEnumerator DoAnimation()
    {
        GameMgr.Instance.audioMgr.Invoke(OnDeath,0);
        while (timer < animationTime)
        {
            transform.localScale = Vector3.one * animationCurves[0].Evaluate(timer);
            transform.position = new Vector3(transform.position.x , baseY + animationCurves[1].Evaluate(timer));
            for (int i = 0; i < sp.Count; i++)
            {
                sp[i].sortingOrder = baseOrder[i] + (int)(animationCurves[2].Evaluate(timer));
            }
            yield return null;
            timer += Time.deltaTime; 
        }
        Destroy(gameObject);
    }
}
