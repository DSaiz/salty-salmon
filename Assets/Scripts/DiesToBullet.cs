﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiesToBullet : MonoBehaviour {

    private bool killed;
    private void OnTriggerEnter2D(Collider2D bullet)
    {
        if(bullet.gameObject.layer == LayerMask.NameToLayer("Bullets"))
        {
            Destroy(bullet.gameObject);
            killObject();
        }
    }


    public void killObject()
    {
        if (!killed)
        {
            killed = true;
            GetComponent<Collider2D>().enabled = false;
            GetComponent<DeathAnimation>().StartAnimation();
        }
    }

}
