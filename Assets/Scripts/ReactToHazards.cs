﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class ReactToHazards : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D hazard)
    {
        if (hazard.gameObject.layer == LayerMask.NameToLayer("Hazards"))
        {
            hazard.gameObject.GetComponent<HazardSFX>().PlaySFX();
            GameMgr.Instance.GameOver();
        }
    }
}
