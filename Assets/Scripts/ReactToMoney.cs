﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class ReactToMoney : MonoBehaviour {


    Rigidbody2D rb;
    SalmonMovement sm;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sm = GetComponent<SalmonMovement>();
    }

    void OnTriggerEnter2D(Collider2D dolarCollider)
    {
        if (dolarCollider.CompareTag("Dolar"))
        {
            Destroy(dolarCollider.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D dolarCollider)
    {
        if (dolarCollider.CompareTag("Dolar"))
        {
            Destroy(dolarCollider.gameObject);
        }
    }
}
