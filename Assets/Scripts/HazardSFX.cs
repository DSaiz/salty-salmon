﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardSFX : MonoBehaviour {

    public string sfx;

	public void PlaySFX()
    {
        GameMgr.Instance.audioMgr.Invoke(sfx, 0);
    }
}
