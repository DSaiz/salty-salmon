﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swan : MonoBehaviour {

    [SerializeField]
    Vector2 pointA;
    [SerializeField]
    Vector2 pointB;
    [SerializeField]
    float patrolTime = 2f;

    SpriteRenderer spr;

    float deltaPatrol = 0f;
    bool direction = true;
    // Use this for initialization
    void Start ()
    {
        spr = GetComponent<SpriteRenderer>();
        pointA = new Vector2(transform.position.x, transform.position.y) + pointA;
        pointB = new Vector2(transform.position.x, transform.position.y) + pointB;
        transform.position = pointA;
    }
	
	// Update is called once per frame
	void Update ()
    {
        deltaPatrol += Time.deltaTime;
        if(direction)
        {
            transform.position = Vector2.Lerp(pointA, pointB, deltaPatrol / patrolTime);
            if(deltaPatrol >= patrolTime)
            {
                direction = false;
                deltaPatrol = 0;
                spr.flipX = ((pointA - pointB).x > 0);

            }
        }
        else
        {
            transform.position = Vector2.Lerp(pointB, pointA, deltaPatrol / patrolTime);
            if (deltaPatrol >= patrolTime)
            {
                direction = true;
                deltaPatrol = 0;
                spr.flipX = ((pointB - pointA).x > 0);

            }
        }
	}
}
