﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;



public class EndingManager : MonoBehaviour {

    // Use this for initialization
    public MovieTexture movieTexture;

    void Start () {
        //((MovieTexture)GetComponent<Renderer>().material.mainTexture).Play();
        movieTexture.Play();
    }

    // Update is called once per frame
    //public event EventHandler SomethingHappened;
    void Update () {

        if (!movieTexture.isPlaying)
        {
            SceneManager.LoadScene("MainMenu");
        }
    }


    //void loopPointReached()
    //{
    //    SceneManager.LoadScene("MainMenu");

    //}

    public void skipVideo()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
